#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(MP(x,y) ,z)
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<pii,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;

using namespace std;
ll n , r , h=0;
vector <ll>v;
vector <piii>rod;
ll costs[1000];

bool BEL()
{
    Rep(i ,n)
    {
        Rep(j ,r)
        {
            ll s = rod[j].first.first;
            ll e = rod[j].first.second;
            ll c = rod[j].second;
            if(costs[s] + c < costs[e] && costs[s] != inf)
            {
                costs[e]= costs[s] + c ;
            }
        }
    }
    Rep(j ,r)
        {
            ll s = rod[j].first.first;
            ll e = rod[j].first.second;
            ll c = rod[j].second;
            if(costs[s] + c < costs[e] && costs[s] != inf)
            {
                costs[e]= -inf ;
            }
        }
    return false;
}
int main()
{
 //   Test;
  //  Testout;
    ll cnt=0;
    while(cin>>n)
    {
        v.clear();
        rod.clear();
        Rep(i , n)
        {
            ll x;
            cin>>x;
            v.push_back(x);
            costs[i]= inf;
        }
        cin>>r;
        Rep(i , r)
        {
            ll a , b ;
            cin>>a>>b;
            a--;
            b--;
            ll w = pow(v[b]-v[a] , 3);
            rod.push_back(MPP(a,b,w));
        }
        costs[0]=0;
        ll t;
        BEL();
        cin>>t;
        cout<< "Set #"<< ++cnt <<endl;
        while(t--)
        {
            cin>>h;
            h--;
            if( costs[h]==inf || costs[h]<3 )
                cout<<"?"<<endl;
            else
                cout<<costs[h]<<endl;
        }
    }
    return 0;
}
